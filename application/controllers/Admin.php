<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Absen_model');
	}

	public function index()
	{
		$data['nilai']=$this->Absen_model->nilai();
		$this->load->view('pertemuan2', $data);
		if($this->session->userdata('status')!="login"){
			redirect(base_url());
		}
	}
}
