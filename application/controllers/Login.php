<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()

	{

		parent::__construct();

		$this->load->model('Absen_model');

		$this->load->model('Login_model');

	}

	public function index()

	{

		$data['nilai']=$this->Absen_model->nilai();

		$this->load->view('pertemuan2', $data);

	}

	function aksi_login()

	{

		$username = $this->input->post('username');

		$password = $this->input->post('password');

		$users = array(

			'username' => $username,

			'password' => md5($password)

		);

		$cek = $this->Login_model->cek_login("users", $users)->num_rows();

		if($cek > 0){

			$data_session = array(

				'nama' => $username,

				'status' => "login"

			);


			$this->session->set_userdata($data_session);

			redirect(base_url('admin'));

		}else{

			$this->session->set_flashdata('error','Wrong username or password');

			redirect(base_url('Welcome/login'));

		}

	}

	function logout(){

		$this->session->sess_destroy();

		redirect(base_url());

	}

}