<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('status')=="login"){
			redirect(base_url('admin'));
		}

		// $this->load->view('welcome_message');
		$data['nilai']=$this->absen_model->nilai();
		$this->load->view('halaman_utama', $data);
	}

	public function pert2()
	{
		$this->load->view('pertemuan2');
	}

	public function pert3()
	{
		$data['nilai']=$this->absen_model->nilai();
		$this->load->view('pertemuan2.php', $data);
	}

	public function __construct()
	{
		parent::__construct();
		$this->load->model('absen_model');
	}

	function edit($NPM)
	{
		$data['nilai']=$this->absen_model->update($NPM);
		$this->load->view('update', $data);
	}

	function updatenilai()
	{
		$NPM = $this->input->post('NPM');
		$Nama = $this->input->post('Nama');
		$P_Web = $this->input->post('P_Web');
		$Jarkom_Lan = $this->input->post('Jarkom_Lan');
		$T_Kompilasi = $this->input->post('T_Kompilasi');
		$data=array(
			'NPM' => $NPM,
			'Nama' => $Nama,
			'P_Web' => $P_Web,
			'Jarkom_Lan' => $Jarkom_Lan,
			'T_Kompilasi' => $T_Kompilasi
		);
		$this->absen_model->update_nilai($data, $NPM);
		redirect(base_url());
	}

	function tambah()
	{
		$this->load->view('tambah');
	}

	function tambahdata()
	{
		$NPM = $this->input->post('NPM');
		$Nama = $this->input->post('Nama');
		$P_Web = $this->input->post('P_Web');
		$Jarkom_Lan = $this->input->post('Jarkom_Lan');
		$T_Kompilasi = $this->input->post('T_Kompilasi');
		$data=array(
			'NPM' => $NPM,
			'Nama' => $Nama,
			'P_Web' => $P_Web,
			'Jarkom_Lan' => $Jarkom_Lan,
			'T_Kompilasi' => $T_Kompilasi
		);
		$this->absen_model->tambah_data($data, $NPM);
		redirect(base_url());
	}

	function hapus($NPM)
	{
		$data['nilai']=$this->absen_model->proses_hapus($NPM);
		$this->absen_model->hapus_data($data,$NPM);
		redirect(base_url());
	}

	public function login()
	{
		$this->load->view('login_page');
	}

	public function home()
	{
		$data['nilai']=$this->Absen_model->nilai();
		$this->load->view('pertemuan2.php', $data);
	}
	
}

