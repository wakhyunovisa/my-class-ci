<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absen_model extends CI_Model
{
	function nilai()
	{
		return $this->db->get('nilai');
	}

	function update($NPM)
	{
		$this->db->where('NPM',$NPM);
		return $this->db->get('nilai');
	}

	function update_nilai($data, $NPM)
	{
		$this->db->where('NPM',$NPM);
		$this->db->set($data);
		$this->db->update('nilai');
	}

	function tambah_data($data)
	{
		$this->db->insert('nilai', $data);
	}

	function hapus_data($data, $NPM)
	{
		$this->db->where('NPM',$NPM);
		$this->db->delete('nilai');
	}

	function proses_hapus($NPM)
	{
		$this->db->where('NPM',$NPM);
		return $this->db->get('nilai');
	}
}